import pymysql, time, logging
import pymysql.cursors
from config import Config

def db_connection():
    for attempt in range(10):
        try:
            return pymysql.connect(host=Config.DATABASE_HOST,
                             port=Config.DATABASE_PORT,
                             user=Config.DATABASE_USER,
                             password=Config.DATABASE_PASSWORD,
                             db=Config.DATABASE_DB,
                             charset='utf8mb4',
                             connect_timeout=Config.DATABASE_CONNECTION_TIMEOUT,
                             cursorclass=pymysql.cursors.DictCursor)
        except Exception as e:
            logging.error(e)
            time.sleep(3)
            continue

def get_submission(submission_ID):
    '''
        Function to return a specific  submission
    '''
    sql = 'call get_submission(%s)'
    conn   = db_connection()
    cursor = conn.cursor()

    cursor.execute(sql,(submission_ID,))
    submission = cursor.fetchone()
    
    cursor.close()
    
    if submission is not None:
        submission['submission'] = submission['submission'].hex() if submission['submission'] is not None else None
        submission['template']   = submission['template'].hex() if submission['template'] is not None else None
    
    return submission

def get_submissions(course_assignment_ID):
    '''
        Function to return all unmarked submissions for a specific assignment/course combo
    '''
    sql = 'call get_submissions(%s)'
    conn   = db_connection()
    cursor = conn.cursor()

    cursor.execute(sql,(course_assignment_ID,))
    submissions = cursor.fetchall()
    
    cursor.close()
    

    for index, submission in enumerate(submissions):
        submission['submission'] = submission['submission'].hex() if submission['submission'] is not None else None
        submission['template']   = submission['template'].hex() if submission['template'] is not None else None
        
        submissions[index] = submission
            
    return submissions



def add_grade(submission_ID, template_ID, grade, total, feedback):
    '''
        Function to add a grade for a specific submission tested with a specific template
    '''
    sql = 'call add_grade(%s, %s, %s, %s, %s)'
    conn   = db_connection()
    cursor = conn.cursor()
    
    try:
        cursor.execute(sql,(submission_ID, template_ID, grade, total, feedback))
    except:
        pass
    conn.commit()
    return
