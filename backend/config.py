import os

here = os.path.dirname(os.path.realpath(__file__))

TEMPLATE_FOLDERS = [here + '/app/templates',
                    here + '/app/templates/main_pages',
                    here + '/app/templates/base_templates',
                    here + '/app/templates/error_pages'
                   ]

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    DATABASE_HOST = os.environ.get('DATABASE_HOST')
    DATABASE_USER = os.environ.get('DATABASE_USER')
    DATABASE_DB   = os.environ.get('DATABASE_DB')
    DATABASE_PASSWORD = os.environ.get('DATABASE_PASSWORD')
    DATABASE_CONNECTION_TIMEOUT = 60
    DATABASE_PORT       = int( os.environ.get('DATABASE_PORT') )
    TESTING_ENGINE_HOST = os.environ.get('TESTING_ENGINE_HOST')
    TESTING_ENGINE_PORT = int( os.environ.get('TESTING_ENGINE_PORT') )
    TESTING_ENGINE = 'http://{}:{}'.format(TESTING_ENGINE_HOST, TESTING_ENGINE_PORT)
    
