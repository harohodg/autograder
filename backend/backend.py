import time, requests, asyncio, logging, json
from database import functions as database_functions
from config import Config


SUBMISSION_URL = '{}/submit_job'.format(Config.TESTING_ENGINE)
JOB_STATUS_URL = '{}/get_job_status'.format(Config.TESTING_ENGINE) + '?job_ID={}'

logging.basicConfig(level=logging.INFO)

#Get database connection


pending_jobs = []


async def get_unmarked_submissions():
    while True:
        db_connection = database_functions.db_connection()
        cursor = db_connection.cursor()
        n = cursor.execute('SELECT * FROM Unmarked_Submissions')
        if n != 0:
            logging.info('Found {} unmarked submissions'.format(n))
        for result in cursor:
            submission_ID = result['submissionID']
            template_ID   = result['templateID']
            submission    = result['submission']
            testing_code  = result['template']
            queue         = result['queue']
            
            matches = [ job for job in pending_jobs if job[0] == submission_ID and job[1] == template_ID ]
            if len(matches) == 0:
                logging.info('Found a file not currently pending')
                #Send this information to test engine
                files = {'submission' : submission, 'testing_code' : testing_code}
                values = {'queue' : queue}
                
                r = requests.post(SUBMISSION_URL, files=files, data=values)
                results = r.json()
                if results['success'] == True:
                    logging.info('Submitted job for testing')
                    job_ID = results['job_ID']
                    pending_jobs.append( (submission_ID, template_ID, job_ID) )
                else:
                    logging.error('Failed to upload job for testing because'.format(results['message']))
                
        
        cursor.close()
        db_connection.close()
        await asyncio.sleep(5)
    return
    
async def get_job_stats():
    while True:
        job_index = 0
        while job_index < len(pending_jobs):
            job = pending_jobs[job_index]
            job_ID = job[-1]
            
            logging.info("Checking the status of job_ID = {}".format(job_ID))
            #Query test engine for job_status
            
            r = requests.get(JOB_STATUS_URL.format(job_ID) )
            results = r.json()
            if results['status'] == 'EXPIRED':
                pending_jobs.pop( job_index )
            else:
                submission_ID = job[0]
                template_ID   = job[1]
                
                if results['status'] == 'FINISHED':
                    logging.info('JOB {} finished with results = {}'.format(job_ID, results['results']) )
                    
                    job_results = json.loads(results['results'])
                    feedback    = job_results['feedback']
                    grade       = job_results['grade']
                    total       = job_results['total']
                    
                    pending_jobs.pop( job_index )
                    
                    database_functions.add_grade(submission_ID, template_ID, grade, total, feedback)
                elif results['status'] == 'FAILED':
                    logging.error('JOB {} FAILED because {})'.format(job_ID, results['results']) )
                    database_functions.add_grade(submission_ID, template_ID, None, None, results['results'])
                    
                    pending_jobs.pop( job_index )
                else:
                    job_index += 1
            
            
            
            await asyncio.sleep(0.1)
        await asyncio.sleep(5)
        pass
    return

async def main():
    await asyncio.gather(get_unmarked_submissions(), get_job_stats())


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    result = loop.run_until_complete(main())
