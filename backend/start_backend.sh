export DOCKER_HOST_IP=$(route -n | awk '/UG[ \t]/{print $2}')

echo $DOCKER_HOST_IP    DOCKER_HOST_IP >> /etc/hosts

python3 backend.py
