import os

here = os.path.dirname(os.path.realpath(__file__))

TEMPLATE_FOLDERS = [here + '/app/templates',
                    here + '/app/templates/main_pages',
                    here + '/app/templates/base_templates',
                    here + '/app/templates/error_pages'
                   ]

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    MAX_CONTENT_LENGTH = 10 * 1024 * 1024
    DATABASE_HOST = os.environ.get('DATABASE_HOST')
    DATABASE_USER = os.environ.get('DATABASE_USER')
    DATABASE_DB   = os.environ.get('DATABASE_DB')
    DATABASE_PASSWORD = os.environ.get('DATABASE_PASSWORD')
    DATABASE_CONNECTION_TIMEOUT = 60
    DEBUG = False
    DATABASE_PORT = int( os.environ.get('DATABASE_PORT') )
