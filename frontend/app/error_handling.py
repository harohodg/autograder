from app import app
from flask import render_template
from app.functions.template_data import get_template_data


@app.errorhandler(401)
def unathenticated(error):
    page = '401'
    return render_template('{}.html'.format(page), **get_template_data(page)), 401
    
@app.errorhandler(403)
def unathenticated(error):
    page = '403'
    return render_template('{}.html'.format(page), **get_template_data(page)), 403
    
@app.errorhandler(404)
def not_found_error(error):
    page = '404'
    return render_template('{}.html'.format(page), **get_template_data(page)), 404
    
    
@app.errorhandler(405)
def bad_request(error):
    page = '405'
    return render_template('{}.html'.format(page), **get_template_data(page)), 405
    
@app.errorhandler(Exception)
def uncaught_error(error):
    page = '500'
    raise error
    return render_template('{}.html'.format(page), **get_template_data(page)), 500
