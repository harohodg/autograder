from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired
from app.functions.permissions import Permissions
import string, random

class LoginForm(FlaskForm):
    username    = StringField('Username', validators=[DataRequired()])
    permissions = StringField('Permissions', validators=[DataRequired()])
    submit = SubmitField('Sign In')
    
    
class NewUserForm(FlaskForm):
    random.seed(a=None, version=2)
    random_id = random.randint(100000000,999999999)
    user_name = ''.join( random.choice(string.ascii_lowercase) for i in range(4) ) + str(random_id)[-4:] 
    
    userName    = StringField('Username', validators=[DataRequired()], default = user_name)
    userID      = StringField('user ID', validators=[DataRequired()],  default = random_id)
    submit = SubmitField('Submit User')
    
