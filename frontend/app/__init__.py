import jinja2, os
from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension
from app.api import bp as api_bp
from config import Config, TEMPLATE_FOLDERS


app = Flask(__name__)
app.jinja_loader = jinja2.FileSystemLoader(TEMPLATE_FOLDERS)
app.config.from_object(Config)
app.register_blueprint(api_bp, url_prefix='/api')

toolbar = DebugToolbarExtension(app)

from app import routes, error_handling
