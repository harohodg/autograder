/*
    Functions to use with the userpermissions table
*/



/*
    Function to update row if course checkbox is clicked.
*/
function updateRow(event){
   $(event.target).parents('tr').find('.permission').prop('checked', event.target.checked) ;
}

/*
    Function to add a course if a permission is selected
*/
function addCourse(event){
   $(event.target).parents('tr').find('.course').prop('checked', true) ;
}


/*
    Function to remove update admin checkbox when a permission
    is selected or removed.
*/
function updateAdmin(event){
    var is_admin = true; 
    $( event.target ).parents('tr').find(".permission").each( function(index){
        is_admin = is_admin && $( this ).prop('checked');
    });
    $(event.target).parents('tr').find("input[name='is_admin']").prop('checked', is_admin);
}


/*
    Function to pack the data and send it back to the server
    when the submit button is clicked.
*/
function submitUserInfo(event){
    event.preventDefault();
    
    var data_to_send = {'userName' : $.trim( $("input[name='userName']").val() ),
                        'userID'   : $.trim( $("input[name='userID']").val() ),
                        'courses'  : [] 
                       };
    
    $( ".course" ).each( function( index ) {
        var course = {};
        if ( $( this ).prop('checked') ){
            course['courseID'] = $( this ).val();
            course['permissions'] = {};
            $( this ).parents('tr').find('.permission').each(function( index ) {
                if ( $( this ).prop('checked') ){
                    course['permissions'][ $( this ).val() ] = true;
                }
            })
            data_to_send['courses'].push(course);
        }
    })                   
                         
    $.ajax({
            type : 'POST',
            url  :'actions/addUser.php',
            cache:false,
            data:data_to_send,
            success:function(response){},
            error:function(xhr,status,error){},
            complete:function(){}
            });
}
