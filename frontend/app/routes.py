from jinja2.exceptions import TemplateNotFound
from flask import render_template, redirect, abort
from app import app

import logging

from app.functions.page_validation import is_authenticated
from app.functions.template_data import get_template_data

VALID_PAGES = ['login', 
               'help',
               'home',
               'courses',
               'course',
               'assignments',
               'assignment',
               'users',
               'user',
               'logout',
               'system'
              ]

@app.route('/<page>')
def general_page(page):
    if page not in VALID_PAGES:
        abort(404)
    elif is_authenticated() == False and page != 'login':
        abort(401)
        
    template_root = 'main_pages' if page in VALID_PAGES else 'error_pages'
    return render_template('{}.html'.format(page), **get_template_data(page))
    
@app.route('/')
def root():
    return redirect('/home')


