import json, pika

def get_connection():
    connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='rabbitmq', heartbeat=0, retry_delay=5, connection_attempts=12 ))
    return connection


def get_channel():
    connection = get_connection()
    channel = connection.channel()
    return channel
    
def send_message(queue, message, peristant=True):
    
    channel = get_channel()

    channel.queue_declare(queue=queue, durable=True)

    rabbit = channel.basic_publish(
                            exchange='',
                            routing_key=queue,
                            body=json.dumps(message),
                            properties=pika.BasicProperties(
                                delivery_mode=2,  # make message persistent
                            ))
    channel.close()
