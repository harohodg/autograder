import mysql.connector, time, logging
from config import Config

def db_connection():
    for attempt in range(10):
        try:
            return mysql.connector.connect(host=Config.DATABASE_HOST,
                             user=Config.DATABASE_USER,
                             password=Config.DATABASE_PASSWORD,
                             database=Config.DATABASE_DB,
                             charset='utf8mb4',
                             port=Config.DATABASE_PORT,
                             connect_timeout=Config.DATABASE_CONNECTION_TIMEOUT)
        except Exception as e:
            logging.error(e)
            time.sleep(5)
            continue


def get_user(user_name):
    '''
        Function to check if user is in database
        Returns None if not found else, userName, permissions
    '''
    sql = 'call get_user(%s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql,(user_name,))
    user = cursor.fetchone()
    
    cursor.close()
    return user;

def get_all_users():
    '''
        Function to return all users in database with their default permissions
    '''
    sql = 'call get_all_users()'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)
    
    cursor.execute(sql)
    users = cursor.fetchall()
    
    cursor.close()
    return users

def get_classlists(user_name):
    '''
        Function to return b'\x01'list of users in courses userName has access to
    '''
    sql = 'call get_user_classlists(%s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)
    
    cursor.execute(sql,(user_name,))
    classlists = cursor.fetchall()
    
    cursor.close()
    return classlists

def get_courses_permissions(username, current=True, future=True, past=True):
    '''
        Function to return list of courses for a specific user and their permissions in said course
    '''    
    sql = 'call get_courses_permissions(%s, %s, %s, %s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql, (username, current, future, past))
    courses_permissions = cursor.fetchall()
         
    cursor.close()
    return courses_permissions
        
def get_courses():
    '''
        Function to return list of all courses on the system.
    '''
    sql = 'SELECT * FROM Courses'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql)
    courses = cursor.fetchall()
    
    cursor.close()    
    return courses
    
def get_course(course_ID):
    '''
        Function to return information about a specific course
    ''' 
    sql = 'CALL get_course(%s)'
    
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql, (course_ID,))
    course = cursor.fetchone()
    
    cursor.close()    
    return course  
    
        
def get_user_assignments(username, current=True, future=False, past=False):
    '''
        Function to return a list of users assignments
    '''
    sql = 'call get_user_assignments(%s, %s, %s, %s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql, (username, current, future, past))
    user_assignments = cursor.fetchall()
    
    cursor.close()
    return user_assignments


def get_testing_units():
    '''
        Function to get list of testing units
    '''
    sql = 'SELECT * FROM TestingUnits'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql)
    testing_units = cursor.fetchall()
    
    cursor.close()    
    return testing_units

def get_assignment(assignment_ID):
    '''
        Function to retrieve an assignment associated with a specific course
    '''
    sql = 'call get_assignment(%s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql, (assignment_ID,))
    assignment = cursor.fetchone()
    
    cursor.close() 
    return assignment


def get_user_pending_submissions(user_name, assignment_ID):
    '''
        Function to retrieve user pending submissions for a specific assignment
    '''
    sql = 'call get_user_pending_submissions(%s,%s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql, (user_name, assignment_ID))
    user_pending_submissions = cursor.fetchall()
    
    cursor.close()
    return user_pending_submissions


def get_user_submission_grades(user_name, assignment_ID):
    '''
        Function to retrieve user submission grades
    '''
    sql = 'call get_user_submission_grades(%s,%s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql, (user_name, assignment_ID))
    user_submission_grades = cursor.fetchall()
    
    cursor.close()
    return user_submission_grades
        
            

        
def add_user(user_name, user_ID):
    sql = 'INSERT INTO `Users` (`userID`, `userName`) VALUES (%s,%s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)
    
    cursor.execute(sql,(user_ID,user_name))
    conn.commit()
    return True

    
    
    
def add_course(course_name, course_url, course_term, course_year):
    sql = 'INSERT INTO `Courses` (`courseName`, `courseURL`, `term`, `year`) VALUES (%s,%s, %s, %s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql, (course_name, course_url, course_term, course_year))
    
    cursor.close()
    conn.commit()
    return

        
def add_course_to_term(course_name, term, year):
    sql = 'INSERT INTO `Course_Term_Year` (`courseName`, `term`, `year`) VALUES (%s,%s, %s)'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql,(course_name, term, year))
    
    cursor.close()
    conn.commit()
    return True


def add_assignment(parameters):
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)
    if parameters['testingTemplate'] != '':
        cursor.execute('INSERT INTO `TestingCode` (`code`) VALUES (%s)', (parameters['testingTemplate'],) )
        testing_code_ID = cursor.lastrowid
    else:
        testing_code_ID = None
        
    course_ID       = parameters['courseID']
    testing_unit    = parameters['testingUnit']
    assignment_name = parameters['assignmentName']
    assignment_url  = parameters['assignmentURL']
    start_date      = parameters['startDate']
    due_date        = parameters['dueDate']
    late_policy     = parameters['latePolicy']
    submissions_delay = parameters['submissionsDelay'] 
    grade_visibility  = parameters['gradeVisibility']
    max_submissions   = parameters['maxSubmissions']

    assignment_data = (course_ID, testing_unit, assignment_name, assignment_url, start_date, due_date, late_policy, submissions_delay, grade_visibility, max_submissions, testing_code_ID)
    sql = 'INSERT INTO `Assignments` (`courseID`,`testingUnit`,`assignmentName`,`assignmentURL`,`startDate`,`dueDate`,`latePolicy`,`submissionsDelay`,`gradeVisibility`,`maxSubmissions`,`testingCode` ) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
    cursor.execute(sql, assignment_data)
    assignment_ID = cursor.lastrowid

    conn.commit()
    return assignment_ID

    
def add_submission(user_name, assignment_ID, file_name, file_guts):
    sql = 'INSERT INTO Submissions (userName, assignmentID, fileName, submission) values (lower(%s), %s, %s, %s);'
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)

    cursor.execute(sql, (user_name, assignment_ID, file_name, file_guts))
    result = cursor.lastrowid

    cursor.close()
    conn.commit()
    return result
    
    
    
def update_assignment(parameters):
    conn   = db_connection()
    cursor = conn.cursor(dictionary=True)
    
    assignment_ID = parameters['assignmentID']
    
    if parameters['testingTemplate'] != '':
        cursor.execute('INSERT INTO `TestingCode` (`code`) VALUES (%s)', (parameters['testingTemplate'],) )
        testing_template_ID = cursor.lastrowid
        
        cursor.execute('UPDATE `Assignments` SET `testingCode` = %s WHERE assignmentID = %s', (testing_template_ID, assignment_ID) )
   

    assignment_name = parameters['assignmentName']
    assignment_url  = parameters['assignmentURL']
    start_date      = parameters['startDate']
    due_date        = parameters['dueDate']
    late_policy     = parameters['latePolicy']
    submissions_delay = parameters['submissionsDelay'] 
    grade_visibility  = parameters['gradeVisibility']
    max_submissions   = parameters['maxSubmissions']

    assignment_data = (assignment_name, assignment_url, start_date, due_date, late_policy, submissions_delay, grade_visibility, max_submissions, assignment_ID )
    sql = 'UPDATE `Assignments` SET `assignmentName` = %s, `assignmentURL` = %s, `startDate` = %s, `dueDate` = %s, `latePolicy` = %s,`submissionsDelay` = %s, `gradeVisibility` = %s,`maxSubmissions` = %s WHERE `assignmentID` = %s'
    cursor.execute(sql, assignment_data)

    conn.commit()
    return
