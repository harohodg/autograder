from flask import session, redirect, url_for, request, render_template, jsonify, abort
from app.api import bp
from app.functions.permissions import get_user_permissions
from app.database import functions as database_functions
from werkzeug.utils import secure_filename
import ast, os
from app.api.functions import get_user, validate_form

import traceback



@bp.route('/logout', methods=['GET'])
def logout():
    return redirect('http://www.wlu.ca')


@bp.route('/new_user', methods=['POST'])
def new_user():
    username, permissions = get_user()
    form_parameters = ['userName', 'userID']
    
    try:
        if username is None:
            abort(401)
        elif permissions['add_users'] == False:
            abort(403)
        elif validate_form(form_parameters, request.form) == False:
            abort(402)
        else:
            user_name = request.form['userName']
            user_ID   = request.form['userID']
            database_functions.add_user(user_name, user_ID)
        result = {'success':True}
    except Exception as e: 
        result = {'success' : False, 'messsage' : 'Failed to add user to system because {}'.format(e)}
    return jsonify(result)

    
@bp.route('/new_course', methods=['POST'])
def new_course():
    username, permissions = get_user()
    form_parameters = ['courseName', 'courseURL', 'courseTerm', 'courseYear']

    try:
        if username is None:
            abort(401)
        elif permissions['create_course'] == False:
            abort(403)
        elif validate_form(form_parameters, request.form) == False:
            abort(402)
        else:
            course_name = request.form['courseName']
            course_url  = request.form['courseURL']
            course_term = request.form['courseTerm']
            course_year = request.form['courseYear']

            database_functions.add_course(course_name, course_url, course_term, course_year)
            result = {'success' : True}
    except Exception as e:
        result = {'success' : False, 'messsage' : 'Failed to add course to system because {}'.format(e)}    
    return jsonify(result)

    
@bp.route('/new_assignment', methods=['POST'])
def new_assignment():
    username, permissions = get_user()
    form_parameters = ['assignmentName', 'assignmentURL', 'startDate', 'dueDate', 'latePolicy', 'submissionsDelay', 'gradeVisibility', 'maxSubmissions' ,'testingUnit', 'courseID']

    try:
        if username is None:
            abort(401)
        elif permissions['create_assignment'] == False:
            abort(403)
        elif validate_form(form_parameters, request.form) == False:
            abort(402)
        else:
            assignment_data = {}
            for key in form_parameters:
                assignment_data[key] = request.form[key]
                
            assignment_data['testingTemplate'] = request.files['testingTemplate'].read() if 'testingTemplate' in request.files and request.files['testingTemplate'].filename != '' else ''
    
            assignment_ID = database_functions.add_assignment( assignment_data )
            result = {'success' : True, 'assignment_ID' : assignment_ID}
    except Exception as e:
        result = {'success' : False, 'messsage' : 'Failed to add assignment to system because {}'.format(e)}   
    return jsonify(result)    



@bp.route('/update_assignment', methods=['POST'])
def update_assignment():
    username, permissions = get_user()
    form_parameters = ['assignmentName', 'assignmentURL', 'startDate', 'dueDate', 'latePolicy', 'submissionsDelay', 'gradeVisibility', 'maxSubmissions', 'assignmentID']

    try:
        if username is None:
            abort(401)
        elif permissions['create_assignment'] == False:
            abort(403)
        elif validate_form(form_parameters, request.form) == False:
            abort(402)
        else:
            assignment_data = {}
            for key in form_parameters:
                assignment_data[key] = request.form[key]
                
            assignment_data['testingTemplate'] = request.files['testingTemplate'].read()   if 'testingTemplate' in request.files and request.files['testingTemplate'].filename != '' else ''
            database_functions.update_assignment( assignment_data )

            result = {'success' : True}
    except Exception as e:
        result = {'success' : False, 'messsage' : 'Failed to update assignment because {}'.format(e)}   
    return jsonify(result)    






@bp.route('/upload_submission', methods=['POST'])
def upload_submission():
    username, permissions = get_user()
    form_parameters = ['file']

    try:
        if username is None:
            abort(401)
        elif permissions['create_assignment'] == False:
            abort(403)
        elif 'file' not in request.files:
            result = {'success' : False, 'messsage' : 'No file submitted'  }
        elif  request.files['file'].filename == '':
            result = {'success' : False, 'messsage' : 'No filename provided'  }
        elif not request.files['file'].filename.endswith('.zip'):
            result = {'success' : False, 'messsage' : 'File is not a zip file.'  }
        else:
            assignment_ID = request.args.get('assignmentID')
 
            file_name     = request.files['file'].filename
            file_data     = request.files['file'].read() 
            submission_ID = database_functions.add_submission( username, assignment_ID, file_name, file_data)
        
            result = {'success' : True, 'submission_ID':submission_ID}
    except Exception as e:
        result = {'success' : False, 'messsage' : 'Failed to upload submission to system because {}'.format(e)  }
    return jsonify(result)  
