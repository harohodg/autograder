from flask import request
from app.functions.permissions import get_user_permissions
from app.database import functions as database_functions


def get_user():
    if request.authorization is None:
        return (None, None)
    else:
        username = getattr(request.authorization, 'username', '')
        user     = database_functions.get_user(username) 
        
        username    = user['userName'] if user is not None else username
        permissions = get_user_permissions( user['permissions'] ) if user is not None else {}
        
        return username, permissions
        
def validate_form(parameters, form):
    for key in parameters:
        if key not in form:
            return False
    return True
