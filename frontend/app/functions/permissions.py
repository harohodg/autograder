'''
Stuff for handling permissions
'''

class Permissions(object):
    EDIT_TEST_SERVERS = 9    
    VIEW_SYSTEM       = 8
    ADD_USERS         = 7
    CREATE_COURSE     = 6
    RUN_MOSS          = 5
    CREATE_ASSIGNMENT = 4
    GRANT_EXTENTION   = 3
    EXPORT_GRADES     = 2
    VIEW_SUBMISSIONS  = 1
    VIEW_ASSIGNMENT_STATS = 0

def get_user_permissions(permissions):
    user_permissions = {}
    is_admin   = True
    is_student = True
    
    for key in [k for k in vars(Permissions) if not k.startswith('__')]:
        has_permission = ( permissions & (1 << getattr(Permissions, key)) ) != 0
        user_permissions[key.lower()] = has_permission
        is_admin   = is_admin and has_permission
        is_student = is_student and not has_permission
        
    user_permissions['is_admin']   = is_admin
    user_permissions['is_student'] = is_student
    
    return user_permissions
