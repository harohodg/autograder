'''
Functions to validate pages.
'''

from flask import request

def is_authenticated():
    '''
        Function to check if user is authenticated.
        
        Returns true/false as appropriated
    '''
    return request.authorization is not None
    

