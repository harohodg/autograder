'''
Function to populate the template data.
'''

from flask import session, request, abort
from app.database import functions as database_functions
from app.functions.permissions import get_user_permissions
import datetime, os



def get_template_data(page):
    page_data = {}
    username      = getattr(request.authorization, 'username', '')
    user          = database_functions.get_user(username) 
    authenticated = username != ''
    
    username = user['userName'] if user is not None else username
    permissions = get_user_permissions( user['permissions'] ) if user is not None else {}
    
    if page == 'home':
        page_data = home_page(username)
    elif page == 'users':
        page_data = users_page(username, permissions)
    elif page == 'courses':
        page_data = courses_page(username)
    elif page == 'course':
        page_data = course_page(username)    
    elif page == 'assignments':
        page_data = assignments_page(username)
    elif page == 'assignment':
        page_data = assignment_page(username)
    elif page == 'system':
        page_data = system_page()    
        
    return {**page_data, **common_data(authenticated, username, permissions), 'title' : page.capitalize() }


def common_data(authenticated, username, permissions):
    return {'authenticated' : authenticated, 'username' : username, 'permissions' : permissions }

def login_page():
    return {}
    
def home_page(username):
    user_assignments = database_functions.get_user_assignments(username, True, False, False);
    return {'user_assignments':user_assignments}
    
def users_page(username, permissions):
    if permissions['is_admin']:
        classlists = database_functions.get_all_users()
    else:
        classlists = database_functions.get_classlists(username)
    
    return {'classlists':classlists}
        
def courses_page(username):
    user_courses = database_functions.get_courses_permissions(username, True, True, False);
    all_courses  = database_functions.get_courses()
    
    now       = datetime.datetime.now()
    this_year = now.year
    return {'user_courses':user_courses, 'all_courses':all_courses, 'this_year':this_year} 

def course_page(username):
    course       = database_functions.get_course(request.args.get('course_ID'))
    user_courses = database_functions.get_courses_permissions(username, True, True, True);
    if course is None:
        abort(404)
    elif len([c for c in user_courses if c['courseID'] == course['courseID'] ]) == 0:
        abort(403)
    
    user_assignments = database_functions.get_user_assignments(username, True, False, False);
    return {'course':course, 'user_assignments':user_assignments} 


def assignments_page(username):
    user_courses     = database_functions.get_courses_permissions(username, True, True, False);
    testing_units    = database_functions.get_testing_units();
    user_assignments = database_functions.get_user_assignments(username, True, False, True);
    return {'user_courses':user_courses, 'user_assignments':user_assignments, 'testing_units':testing_units}    

def assignment_page(username):
    assignment_ID    = request.args.get('assignment_ID')
    assignment       = database_functions.get_assignment( assignment_ID )
    user_assignments = database_functions.get_user_assignments(username, True, True, True);
    if assignment is None:
        abort(404)
    elif len([a for a in user_assignments if a['assignmentID'] == assignment['assignmentID'] ]) == 0:
        abort(403)
    
    user_pending_submissions = database_functions.get_user_pending_submissions(username, assignment_ID)
    user_submission_grades   = database_functions.get_user_submission_grades(username, assignment_ID)
    return {'assignment': assignment, 'user_submission_grades':user_submission_grades, 'user_pending_submissions': user_pending_submissions}
    
    
def system_page():
    host = DATABASE_DB = os.environ.get('HOST')
    return {'host' : host}       
