export DOCKER_HOST_IP=$(route -n | awk '/UG[ \t]/{print $2}')

echo $DOCKER_HOST_IP    DOCKER_HOST_IP >> /etc/hosts

gunicorn --bind 0.0.0.0:8000 frontend
