Welcome to **project 43** [^1] , an auto-grading framework developed at [Wilfrid Laurier University](https://www.wlu.ca) by [Harold Hodgins](https://www.mypurplecrayon.ca) for the Computer Science Department. 
Initially Project 43 will support testing programming assignments done in C, Java and Python [^2]. A look behind the curtain can be found [here](https://bitbucket.org/harohodg/autograder/wiki/Home).

The general premise is to provide a black box where files go in and marks come out. On a more granular level files go in and testing results/grades come out. 
For the Beta version this means providing the black box with the file(s) to test and the file(s) to run which do the actual testing. The black box just provides a sandbox to do this in. 
Project 43 provides a layer of wrapping paper around the black box which makes it look nice and pretty and provides additional functionallity such as storing information about courses, and assigments and grades for specific users with specific permmissions.

We are using [Project 44](https://bitbucket.org/harohodg/project-44/src) as the black box testing engine. 

## General Usage
To be filled in.

## Deployment
Project 43 is implemented as collection of [Docker](https://www.docker.com/) containers including the [testing engine](https://bitbucket.org/harohodg/project-44/src) therefore it is assumed that if you want to use Project 43 you have Docker and [Docker Compose](https://docs.docker.com/compose/) installed [^3]. We also recommend running a Docker GUI such as [Portainer](https://www.portainer.io/)[^4] to control the Project 43 containers. Of course you will also need [Project 44](https://bitbucket.org/harohodg/project-44/src) running somewhere.

We recommend deploying Project 43 on its own system with a reasonable amount of available resources[^5] to avoid conflicts[^6] but you can run in on an existing system.

1) Start by downloading and setting up [Project 44](https://bitbucket.org/harohodg/project-44/src) if not already so done.

2) Download the latest version of project 43 from [here](https://bitbucket.org/harohodg/autograder/downloads/?tab=tags) and extract it somewhere.

3) Edit the .env file in the root folder and change the hostname to http://hostmachine. Change the DATABASE_HOST and TESTING_ENGINE_HOST if they are not on the same machine. Change the values of DATABASE_PORT and TESTING_ENGINE_PORT if necessary.  If using the included database change the database password. 

4) Edit the docker-compose.yml file in the root folder and change change the default memory limits if so desired.

5) If using a local copy of Apache edit the docker-compose.yml file and comment out (add # to the start of the line) the apache service and apache under frontend -> depends on. Configure your Apache to reverse proxy requests to localhost:8000 and require authentication. 

6) If using your own MYSQL database edit the docker-compose.yml file and comment out (add # to the start of the line) the database service.

7) Run `./rebuild.sh` in the folder root to build the required images. After run `./start_autograder.sh` to start the system. run `./stop_autograder.sh` to stop it. This will stop all of the containers. All will get amnesia except for the database

8) If using your own MYSQL database you will need to add a new database called autograder with the appropriate tables, views and stored procedures as defined in database/init_database.sql

9) Once the system is up access the database using your favourite program and add an admin and testing engine information as follows

	- add user ID and user name to Users
	- add same username and 1023 for permissions to User_Permissions
    - add testing units to TestingUnits with a description and queue. The queue must be one of those on the testing engine.

## Versioning
We are trying to follow [semantic versioning](https://semver.org/) rules for versioning ezmarker. This program is currently a beta version v0.0.0. It is functional but and usable but needs some work before being released for general use. As is it is ~safe~ for public usage but I wouldn't count on it for critical applications. It is of course offered "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. 



[^1]: Name likely to change before release into the wild.

[^2]: Tecnically it can test/mark any assignment if it's provided with the correct files

[^3]: It is recommended that you set Docker up to not require root access to run it. See [here](https://docs.docker.com/install/linux/linux-postinstall/) for details. 

[^4]: Included as part of project 43 since it's just another container.

[^5]: Runs fine on a laptop with 8GB of ram and an 8GB swap file. Had issues running on a Digital Ocean machine with 1GB of RAM and no swap . Database kept running out of memory even after optimizing.

[^6]: We've had docker cripple a running server on several occasions for now apparent reason.
