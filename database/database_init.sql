CREATE DATABASE  IF NOT EXISTS `autograder` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `autograder`;
-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: autograder
-- ------------------------------------------------------
-- Server version	5.6.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `ALL_Submissions`
--

DROP TABLE IF EXISTS `ALL_Submissions`;
/*!50001 DROP VIEW IF EXISTS `ALL_Submissions`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ALL_Submissions` AS SELECT 
 1 AS `submissionID`,
 1 AS `assignmentID`,
 1 AS `userName`,
 1 AS `timeStamp`,
 1 AS `fileName`,
 1 AS `submission`,
 1 AS `queue`,
 1 AS `testCodeID`,
 1 AS `testingCode`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `Admins`
--

DROP TABLE IF EXISTS `Admins`;
/*!50001 DROP VIEW IF EXISTS `Admins`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `Admins` AS SELECT 
 1 AS `userName`,
 1 AS `permissions`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `All_Assignments`
--

DROP TABLE IF EXISTS `All_Assignments`;
/*!50001 DROP VIEW IF EXISTS `All_Assignments`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `All_Assignments` AS SELECT 
 1 AS `courseID`,
 1 AS `courseName`,
 1 AS `term`,
 1 AS `year`,
 1 AS `courseStartDate`,
 1 AS `courseEndDate`,
 1 AS `displayedTerm`,
 1 AS `assignmentID`,
 1 AS `assignmentName`,
 1 AS `assignmentURL`,
 1 AS `assignmentStartDate`,
 1 AS `assignmentDueDate`,
 1 AS `latePolicy`,
 1 AS `submissionsDelay`,
 1 AS `maxSubmissions`,
 1 AS `gradeVisibility`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `All_Courses`
--

DROP TABLE IF EXISTS `All_Courses`;
/*!50001 DROP VIEW IF EXISTS `All_Courses`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `All_Courses` AS SELECT 
 1 AS `courseID`,
 1 AS `courseName`,
 1 AS `courseURL`,
 1 AS `term`,
 1 AS `year`,
 1 AS `startDate`,
 1 AS `endDate`,
 1 AS `displayedTerm`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `All_User_Course_Permissions`
--

DROP TABLE IF EXISTS `All_User_Course_Permissions`;
/*!50001 DROP VIEW IF EXISTS `All_User_Course_Permissions`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `All_User_Course_Permissions` AS SELECT 
 1 AS `userName`,
 1 AS `courseID`,
 1 AS `permissions`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Assigment_Extentions`
--

DROP TABLE IF EXISTS `Assigment_Extentions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Assigment_Extentions` (
  `userName` varchar(10) NOT NULL,
  `assigmentID` int(10) unsigned NOT NULL,
  `extention` time NOT NULL,
  PRIMARY KEY (`userName`,`assigmentID`),
  KEY `fk_Assigment_Extentions_2_idx` (`assigmentID`),
  CONSTRAINT `fk_Assigment_Extentions_1` FOREIGN KEY (`userName`) REFERENCES `Users` (`userID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_Assigment_Extentions_2` FOREIGN KEY (`assigmentID`) REFERENCES `Assignments` (`assignmentID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Assignments`
--

DROP TABLE IF EXISTS `Assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Assignments` (
  `assignmentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `courseID` int(10) unsigned NOT NULL,
  `assignmentName` varchar(45) NOT NULL,
  `assignmentURL` varchar(45) NOT NULL,
  `startDate` datetime NOT NULL,
  `dueDate` datetime NOT NULL,
  `latePolicy` bit(1) NOT NULL DEFAULT b'1',
  `submissionsDelay` smallint(4) unsigned NOT NULL DEFAULT '0',
  `gradeVisibility` bit(1) NOT NULL DEFAULT b'0',
  `maxSubmissions` tinyint(3) unsigned NOT NULL,
  `testingUnit` int(10) unsigned NOT NULL,
  `testingCode` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`assignmentID`),
  KEY `fk_Assignments_1_idx` (`courseID`),
  KEY `fk_Assignments_2_idx` (`testingUnit`),
  KEY `fk_Assignments_3_idx` (`testingCode`),
  CONSTRAINT `fk_Assignments_1` FOREIGN KEY (`courseID`) REFERENCES `Courses` (`courseID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Assignments_2` FOREIGN KEY (`testingUnit`) REFERENCES `TestingUnits` (`testUnitID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Assignments_3` FOREIGN KEY (`testingCode`) REFERENCES `TestingCode` (`testCodeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Courses`
--

DROP TABLE IF EXISTS `Courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Courses` (
  `courseID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `courseName` varchar(50) NOT NULL,
  `term` enum('Winter','Spring','Summer','Fall') NOT NULL,
  `year` smallint(4) NOT NULL,
  `courseURL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`courseID`),
  KEY `fk_Course_Term_Name_1_idx` (`courseName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `Default_User_Permissions`
--

DROP TABLE IF EXISTS `Default_User_Permissions`;
/*!50001 DROP VIEW IF EXISTS `Default_User_Permissions`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `Default_User_Permissions` AS SELECT 
 1 AS `userName`,
 1 AS `permissions`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Grades`
--

DROP TABLE IF EXISTS `Grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Grades` (
  `submissionID` int(10) unsigned NOT NULL,
  `templateID` int(10) unsigned NOT NULL,
  `grade` int(11) DEFAULT NULL,
  `total` int(10) unsigned DEFAULT NULL,
  `feedback` text NOT NULL,
  PRIMARY KEY (`submissionID`,`templateID`),
  KEY `fk_Grades_2_idx` (`templateID`),
  CONSTRAINT `fk_Grades_1` FOREIGN KEY (`submissionID`) REFERENCES `Submissions` (`submissionID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Grades_2` FOREIGN KEY (`templateID`) REFERENCES `TestingCode` (`testCodeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Moss_Credentials`
--

DROP TABLE IF EXISTS `Moss_Credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Moss_Credentials` (
  `userName` varchar(10) NOT NULL,
  `mossID` smallint(10) unsigned NOT NULL,
  PRIMARY KEY (`userName`,`mossID`),
  UNIQUE KEY `mossID_UNIQUE` (`mossID`),
  UNIQUE KEY `userName_UNIQUE` (`userName`),
  CONSTRAINT `fk_Moss_Credentials_1` FOREIGN KEY (`userName`) REFERENCES `Users` (`userName`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Submissions`
--

DROP TABLE IF EXISTS `Submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Submissions` (
  `submissionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userName` varchar(10) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `assignmentID` int(10) unsigned NOT NULL,
  `submission` blob NOT NULL,
  `fileName` varchar(45) NOT NULL,
  PRIMARY KEY (`submissionID`),
  UNIQUE KEY `submissionID_UNIQUE` (`submissionID`),
  KEY `fk_Submissions_1_idx` (`userName`),
  KEY `fk_Submissions_2_idx` (`assignmentID`),
  CONSTRAINT `fk_Submissions_1` FOREIGN KEY (`userName`) REFERENCES `Users` (`userName`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Submissions_2` FOREIGN KEY (`assignmentID`) REFERENCES `Assignments` (`assignmentID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TestingCode`
--

DROP TABLE IF EXISTS `TestingCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TestingCode` (
  `testCodeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` blob NOT NULL,
  PRIMARY KEY (`testCodeID`),
  UNIQUE KEY `testCodeID_UNIQUE` (`testCodeID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TestingUnits`
--

DROP TABLE IF EXISTS `TestingUnits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TestingUnits` (
  `testUnitID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(500) NOT NULL,
  `queue` varchar(50) NOT NULL,
  PRIMARY KEY (`testUnitID`),
  UNIQUE KEY `queue_UNIQUE` (`queue`),
  UNIQUE KEY `testUnitID_UNIQUE` (`testUnitID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `Unmarked_Submissions`
--

DROP TABLE IF EXISTS `Unmarked_Submissions`;
/*!50001 DROP VIEW IF EXISTS `Unmarked_Submissions`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `Unmarked_Submissions` AS SELECT 
 1 AS `submissionID`,
 1 AS `assignmentID`,
 1 AS `userName`,
 1 AS `timeStamp`,
 1 AS `fileName`,
 1 AS `submission`,
 1 AS `queue`,
 1 AS `templateID`,
 1 AS `template`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `User_Assignments`
--

DROP TABLE IF EXISTS `User_Assignments`;
/*!50001 DROP VIEW IF EXISTS `User_Assignments`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `User_Assignments` AS SELECT 
 1 AS `assignmentID`,
 1 AS `courseID`,
 1 AS `courseName`,
 1 AS `term`,
 1 AS `year`,
 1 AS `userName`,
 1 AS `assignmentName`,
 1 AS `assignmentURL`,
 1 AS `startDate`,
 1 AS `dueDate`,
 1 AS `latePolicy`,
 1 AS `submissionsDelay`,
 1 AS `gradeVisibility`,
 1 AS `maxSubmissions`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `User_Course_Permissions`
--

DROP TABLE IF EXISTS `User_Course_Permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User_Course_Permissions` (
  `userName` varchar(10) NOT NULL,
  `courseID` int(10) unsigned NOT NULL,
  `permissions` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`userName`,`courseID`),
  KEY `fk_User_Course_Permissions_2_idx` (`courseID`),
  CONSTRAINT `fk_User_Course_Permissions_1` FOREIGN KEY (`userName`) REFERENCES `Users` (`userName`) ON UPDATE CASCADE,
  CONSTRAINT `fk_User_Course_Permissions_2` FOREIGN KEY (`courseID`) REFERENCES `Courses` (`courseID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User_Permissions`
--

DROP TABLE IF EXISTS `User_Permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User_Permissions` (
  `userName` varchar(10) NOT NULL,
  `permissions` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`userName`),
  CONSTRAINT `fk_User_Permissions_1` FOREIGN KEY (`userName`) REFERENCES `Users` (`userName`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `User_Submission_Grade`
--

DROP TABLE IF EXISTS `User_Submission_Grade`;
/*!50001 DROP VIEW IF EXISTS `User_Submission_Grade`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `User_Submission_Grade` AS SELECT 
 1 AS `userName`,
 1 AS `assignmentID`,
 1 AS `submissionID`,
 1 AS `templateID`,
 1 AS `timeStamp`,
 1 AS `fileName`,
 1 AS `grade`,
 1 AS `total`,
 1 AS `feedback`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `userID` varchar(10) NOT NULL,
  `userName` varchar(10) NOT NULL,
  PRIMARY KEY (`userID`,`userName`),
  UNIQUE KEY `userID_UNIQUE` (`userID`),
  UNIQUE KEY `userName_UNIQUE` (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'autograder'
--
/*!50003 DROP FUNCTION IF EXISTS `startDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `startDate`(term varchar(10), course_year int) RETURNS varchar(10) CHARSET utf8mb4
BEGIN
DECLARE start_date char(10);

   IF term = 'Winter' THEN
      SET start_date = CONCAT(course_year, '-01-01');
   ELSEIF term = 'Spring' THEN
      SET start_date = CONCAT(course_year, '-05-01');
   ELSEIF term = 'Summer' THEN
      SET start_date = CONCAT(course_year, '-07-01');
   ELSEIF term = 'Fall' THEN
      SET start_date = CONCAT(course_year, '-09-01');      
   ELSE
      SET start_date = '0000-00-00';
   END IF;

   RETURN income_level;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `TERMENDDATE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `TERMENDDATE`(term varchar(10), course_year int) RETURNS varchar(10) CHARSET utf8mb4
BEGIN
DECLARE end_date char(10);

   IF term = 'Winter' THEN
      SET end_date = CONCAT(course_year, '-04-30');
   ELSEIF term = 'Spring' THEN
      SET end_date = CONCAT(course_year, '-08-31');
   ELSEIF term = 'Summer' THEN
      SET end_date = CONCAT(course_year, '-08-31');
   ELSEIF term = 'Fall' THEN
      SET end_date = CONCAT(course_year, '-12-31');      
   ELSE
      SET end_date = '9999-99-99';
   END IF;

   RETURN end_date;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `TERMSTARTDATE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `TERMSTARTDATE`(term varchar(10), course_year int) RETURNS varchar(10) CHARSET utf8mb4
BEGIN
DECLARE start_date char(10);

   IF term = 'Winter' THEN
      SET start_date = CONCAT(course_year, '-01-01');
   ELSEIF term = 'Spring' THEN
      SET start_date = CONCAT(course_year, '-05-01');
   ELSEIF term = 'Summer' THEN
      SET start_date = CONCAT(course_year, '-07-01');
   ELSEIF term = 'Fall' THEN
      SET start_date = CONCAT(course_year, '-09-01');      
   ELSE
      SET start_date = '0000-00-00';
   END IF;

   RETURN start_date;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_grade` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_grade`(IN submission_ID int, IN template_ID int, IN grade int, IN total int, IN feedback varchar(100))
BEGIN
INSERT INTO `Grades` (`submissionID`, `templateID`, `grade`, `total`, `feedback`) VALUES (submission_ID, template_ID, grade, total, feedback);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_submission` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_submission`(IN user_name varchar(10), IN assignment_ID int, IN file_name varchar(20), IN submission_data blob)
BEGIN
INSERT INTO Submissions (userName, assignmentID, fileName, submission) values (lower(user_name), assignment_ID, file_name, submission_data);
SELECT LAST_INSERT_ID() as submissionID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_all_users`()
BEGIN
	SELECT * FROM autograder.Default_User_Permissions;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_assignment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_assignment`(IN assignment_ID INT)
BEGIN
SELECT * FROM All_Assignments
WHERE assignmentID = assignment_ID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_course` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_course`(IN course_ID int)
BEGIN
	SELECT * FROM Courses where courseID = course_ID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_courses_permissions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_courses_permissions`(IN user_name varchar(25), IN current_courses bit, IN future_courses bit, IN past_courses bit)
BEGIN
SELECT 
Users.userID,
Users.userName,
permissions,
All_Courses.courseID,
All_Courses.courseName,
All_Courses.courseURL,
term,
year,
startDate,
endDate,
displayedTerm

FROM All_Courses JOIN All_User_Course_Permissions
ON All_Courses.courseID = All_User_Course_Permissions.courseID
JOIN Users ON Users.userName = All_User_Course_Permissions.userName
WHERE Users.userName = lower(user_name) and 
(
(current_courses and (startDate <= CURDATE() and endDate >= CURDATE()) )
or
(future_courses and (startDate > CURDATE()) )
or
(past_courses and (endDate < CURDATE()) )
)
order by startDate;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_course_assignments` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_course_assignments`(IN course_ID int)
BEGIN
SELECT * FROM All_Assignments WHERE courseID = course_ID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_submission` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_submission`(IN submission_ID int)
BEGIN
SELECT submissionID,
templateID,
submission,
template
 From Unmarked_Submissions where submissionID = submission_ID and templateID is not null;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_submissions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_submissions`(IN assignment_ID int)
BEGIN
SELECT * FROM Unmarked_Submissions WHERE assignmentID = assignment_ID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_user`(IN user_name varchar(10))
BEGIN

SELECT * FROM Default_User_Permissions 
where userName = lower(user_name);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_assignments` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_user_assignments`(IN user_name varchar(25), IN current_assignments bit, IN future_assignments bit, IN past_assignments bit)
BEGIN
SELECT * FROM User_Assignments
 WHERE userName = lower(user_name) and 
(
(current_assignments and (startDate <= CURDATE() and dueDate >= CURDATE()) )
or
(future_assignments and (startDate > CURDATE()) )
or
(past_assignments and (dueDate < CURDATE()) )
)
order by dueDate;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_classlists` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_user_classlists`(IN user_name varchar(10))
BEGIN
select classlist.userName, permissions from
(
select All_User_Course_Permissions.userName from
(
Select * from All_User_Course_Permissions
where userName = lower(user_name)
) as user_courses
JOIN All_User_Course_Permissions on 
All_User_Course_Permissions.courseID = user_courses.courseID
group by All_User_Course_Permissions.userName
) as classlist
JOIN Default_User_Permissions ON
classlist.userName = Default_User_Permissions.userName
order by permissions desc, userName asc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_pending_submissions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_user_pending_submissions`(IN user_name varchar(10), IN assignment_ID int)
BEGIN
SELECT timeStamp FROM Unmarked_Submissions
WHERE userName = lower(user_name) and assignmentID = assignment_ID
order by timeStamp desc;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_submission_grades` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_user_submission_grades`(IN user_name varchar(10), IN assignment_ID int)
BEGIN
SELECT * FROM User_Submission_Grade
WHERE userName = lower(user_name) and assignmentID = assignment_ID
and templateID IN
(
select MAX(templateID) from User_Submission_Grade
group by submissionID

)
order by timeStamp desc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `ALL_Submissions`
--

/*!50001 DROP VIEW IF EXISTS `ALL_Submissions`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `ALL_Submissions` AS select `Submissions`.`submissionID` AS `submissionID`,`Submissions`.`assignmentID` AS `assignmentID`,`Submissions`.`userName` AS `userName`,`Submissions`.`timeStamp` AS `timeStamp`,`Submissions`.`fileName` AS `fileName`,`Submissions`.`submission` AS `submission`,`TestingUnits`.`queue` AS `queue`,`TestingCode`.`testCodeID` AS `testCodeID`,`TestingCode`.`code` AS `testingCode` from (((`Submissions` join `Assignments` on((`Submissions`.`assignmentID` = `Assignments`.`assignmentID`))) join `TestingUnits` on((`Assignments`.`testingUnit` = `TestingUnits`.`testUnitID`))) join `TestingCode` on((`Assignments`.`testingCode` = `TestingCode`.`testCodeID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `Admins`
--

/*!50001 DROP VIEW IF EXISTS `Admins`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `Admins` AS select `Default_User_Permissions`.`userName` AS `userName`,`Default_User_Permissions`.`permissions` AS `permissions` from `Default_User_Permissions` where (`Default_User_Permissions`.`permissions` = 1023) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `All_Assignments`
--

/*!50001 DROP VIEW IF EXISTS `All_Assignments`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `All_Assignments` AS select `All_Courses`.`courseID` AS `courseID`,`All_Courses`.`courseName` AS `courseName`,`All_Courses`.`term` AS `term`,`All_Courses`.`year` AS `year`,`All_Courses`.`startDate` AS `courseStartDate`,`All_Courses`.`endDate` AS `courseEndDate`,`All_Courses`.`displayedTerm` AS `displayedTerm`,`Assignments`.`assignmentID` AS `assignmentID`,`Assignments`.`assignmentName` AS `assignmentName`,`Assignments`.`assignmentURL` AS `assignmentURL`,`Assignments`.`startDate` AS `assignmentStartDate`,`Assignments`.`dueDate` AS `assignmentDueDate`,`Assignments`.`latePolicy` AS `latePolicy`,`Assignments`.`submissionsDelay` AS `submissionsDelay`,`Assignments`.`maxSubmissions` AS `maxSubmissions`,`Assignments`.`gradeVisibility` AS `gradeVisibility` from (`All_Courses` join `Assignments` on((`All_Courses`.`courseID` = `Assignments`.`courseID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `All_Courses`
--

/*!50001 DROP VIEW IF EXISTS `All_Courses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `All_Courses` AS select `Courses`.`courseID` AS `courseID`,`Courses`.`courseName` AS `courseName`,`Courses`.`courseURL` AS `courseURL`,`Courses`.`term` AS `term`,`Courses`.`year` AS `year`,`TERMSTARTDATE`(`Courses`.`term`,`Courses`.`year`) AS `startDate`,`TERMENDDATE`(`Courses`.`term`,`Courses`.`year`) AS `endDate`,concat(`Courses`.`term`,' ',`Courses`.`year`) AS `displayedTerm` from `Courses` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `All_User_Course_Permissions`
--

/*!50001 DROP VIEW IF EXISTS `All_User_Course_Permissions`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `All_User_Course_Permissions` AS select `User_Course_Permissions`.`userName` AS `userName`,`User_Course_Permissions`.`courseID` AS `courseID`,(case when isnull(`User_Course_Permissions`.`permissions`) then `Default_User_Permissions`.`permissions` else `User_Course_Permissions`.`permissions` end) AS `permissions` from (`User_Course_Permissions` join `Default_User_Permissions` on((`User_Course_Permissions`.`userName` = `Default_User_Permissions`.`userName`))) union all select `Admins`.`userName` AS `userName`,`Courses`.`courseID` AS `courseID`,`Admins`.`permissions` AS `permissions` from (`Admins` join `Courses`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `Default_User_Permissions`
--

/*!50001 DROP VIEW IF EXISTS `Default_User_Permissions`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `Default_User_Permissions` AS select `Users`.`userName` AS `userName`,(case when isnull(`User_Permissions`.`permissions`) then 0 else `User_Permissions`.`permissions` end) AS `permissions` from (`Users` left join `User_Permissions` on((`Users`.`userName` = `User_Permissions`.`userName`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `Unmarked_Submissions`
--

/*!50001 DROP VIEW IF EXISTS `Unmarked_Submissions`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `Unmarked_Submissions` AS select `ALL_Submissions`.`submissionID` AS `submissionID`,`ALL_Submissions`.`assignmentID` AS `assignmentID`,`ALL_Submissions`.`userName` AS `userName`,`ALL_Submissions`.`timeStamp` AS `timeStamp`,`ALL_Submissions`.`fileName` AS `fileName`,`ALL_Submissions`.`submission` AS `submission`,`ALL_Submissions`.`queue` AS `queue`,`ALL_Submissions`.`testCodeID` AS `templateID`,`ALL_Submissions`.`testingCode` AS `template` from (`ALL_Submissions` left join `Grades` on(((`ALL_Submissions`.`submissionID` = `Grades`.`submissionID`) and (`ALL_Submissions`.`testCodeID` = `Grades`.`templateID`)))) where isnull(`Grades`.`templateID`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `User_Assignments`
--

/*!50001 DROP VIEW IF EXISTS `User_Assignments`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `User_Assignments` AS select `Assignments`.`assignmentID` AS `assignmentID`,`Courses`.`courseID` AS `courseID`,`Courses`.`courseName` AS `courseName`,`Courses`.`term` AS `term`,`Courses`.`year` AS `year`,`All_User_Course_Permissions`.`userName` AS `userName`,`Assignments`.`assignmentName` AS `assignmentName`,`Assignments`.`assignmentURL` AS `assignmentURL`,`Assignments`.`startDate` AS `startDate`,`Assignments`.`dueDate` AS `dueDate`,`Assignments`.`latePolicy` AS `latePolicy`,`Assignments`.`submissionsDelay` AS `submissionsDelay`,`Assignments`.`gradeVisibility` AS `gradeVisibility`,`Assignments`.`maxSubmissions` AS `maxSubmissions` from ((`All_User_Course_Permissions` join `Assignments` on((`All_User_Course_Permissions`.`courseID` = `Assignments`.`courseID`))) join `Courses` on((`Courses`.`courseID` = `Assignments`.`courseID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `User_Submission_Grade`
--

/*!50001 DROP VIEW IF EXISTS `User_Submission_Grade`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `User_Submission_Grade` AS select `Submissions`.`userName` AS `userName`,`Submissions`.`assignmentID` AS `assignmentID`,`Submissions`.`submissionID` AS `submissionID`,`Grades`.`templateID` AS `templateID`,`Submissions`.`timeStamp` AS `timeStamp`,`Submissions`.`fileName` AS `fileName`,`Grades`.`grade` AS `grade`,`Grades`.`total` AS `total`,`Grades`.`feedback` AS `feedback` from (`Grades` join `Submissions` on((`Submissions`.`submissionID` = `Grades`.`submissionID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-16 12:53:26
